# NQueens
---------
NQueens is a program which solves the problem for placing N number of chess queens on an N by N chessboard so that no two queens attack one another. 
Instead of using recursion to solve the problem, the program is implemented using a stack. 
A set of pre-positioned queens can be given in the INPUT FILE. The program will then find the coordinates of the remaining queens on the board. If the pre-positioned queens don't have a solution, then the program will output "no solution".

## Files
--------
These are the files associated with the program

- Code files: Makefile, main.cpp, Queen.cpp, Queen.h
- Test files: simple-input.txt, simple-output.txt, more-input.txt, more-output.txt
- Extra files: README

## Usage
--------
- Run "make", to get executable "nqueens".
- Run "./nqueens <INPUT FILE> <OUTPUT FILE>"

Each line of the INPUT FILE should have the board size, a non-negative integer.
Coordinates for queens is optional.
Each line of OUTPUT file has the numbers in the form <column> <space> <row> <space>
for queens found from the program.

## Built Using
-------------
- C++

## Reference
------------
In code file Queen.cpp, method <bool is_safe(int row int column)> is derived from various online resources that solve the problem, however my function evaluates the entire board for safety, whereas resources online only evaluate columns, rows, and right side of the diagonal.
[primary resource](https://www.geeksforgeeks.org/printing-solutions-n-queen-problem/)

## Author
---------
- Ulfat Fruitwala